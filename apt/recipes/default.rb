#
# Cookbook Name:: apt
# Recipe:: default
#
# Copyright 2017, Cloudaxis Limited
#
# All rights reserved - Do Not Redistribute
#
execute "apt-get update" do
  command "apt-get update"
end


remote_file "#{Chef::Config[:file_cache_path]}/codedeploy-agent-install" do
    source "https://s3.amazonaws.com/aws-codedeploy-ap-southeast-1/latest/install"
    mode 0755
end

bash "install-codedeploy-agent" do
  code <<-EOH
    #{Chef::Config[:file_cache_path]}/codedeploy-agent-install auto
  EOH
end

service "codedeploy-agent" do
	action [:enable, :start]
end



%w{mysql-client curl}.each do |pkg|
   package pkg do
      action :install
   end
end
