#
# Cookbook Name:: souki-market
# Recipe:: default
#
# Copyright 2017, Cloudaxis Limited
#
# All rights reserved - Do Not Redistribute
#
package 'nginx' do
  action :install
end

%w{nginx supervisor}.each do |pkg|
   package pkg do
      action :install
   end
end

service 'nginx' do
  action [ :enable, :start ]
end